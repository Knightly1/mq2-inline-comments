![currentstatus] MQ2 Inline Comments
========================

  # Overview
  
  This updates the macro language by adding the ability to do inline comments (at the end of a line).  The delimiter
  chosen for this is `|@@` so as not to interfere with other aspects of the language.

  ## Documentation

  To use an inline comment inside a macro, start the comment with `|@@`.  If you want to use `|@@` somewhere else, 
  just add a backslash in front of it as escape character.
  
  Some macro examples below:
  ```
  Input :  /echo ${Me.Name}
  Output:  [MQ2] Knightly

  Input :  /echo ${Me.Name} |@@ This is my name
  Output:  [MQ2] Knightly 
  
  Input :  /echo ${Me.Name} \|@@ This is my name
  Output:  [MQ2] Knightly |@@ This is my name

  Input :  /echo ${Me.Name} \|@@ This is my |@@name
  Output:  [MQ2] Knightly |@@ This is my

  Input :  /echo ${Me.Name} |@@ This is my \|@@name
  Output:  [MQ2] Knightly 
  ```
    
  ## What Else
  - Mostly backwards compatible so existing macros should continue to run
  - Will cause issues with macros that use |@@ (but I couldn't find any that did this)
  - The files in this repository are just the changed files (ie, it will not compile on its own)
  - Current as of MQ2-20190731(Test).zip

 ## Example Comment Test

  Below is a test macro demonstrating how to use the new functionality.  To see the difference:
  1. Save the below macro as [TestComments.mac]
  2. Run `/mac testcomments`
  6. Note the comments do not show up in output
  
```
Sub Main
    /echo Starting Test...
    |/echo You should not see this line which is commented.
    |**
    /echo Nor should you see this line which is in a block comment.
    **|
    /echo 1.) You should see this line.
    /echo 2.) You should see this line (again).  |@@ But not this part
    /echo 3.) You should see this line and \|@@ the comment marker.
    /echo 4.) You should not see the comment marker this time |@@ Because the comment marker is behind a comment \|@@
    /echo For the full test suite see TestComments.mac.
/return
```

  [currentstatus]: https://img.shields.io/badge/Status-In%20Beta-orange.svg
  [TestComments.mac]: https://gitlab.com/Knightly1/mq2-inline-comments/raw/master/TestComments.mac?inline=false
  